FROM maven:3-jdk-14

ADD target/api-0.0.1-SNAPSHOT.jar .

EXPOSE 8080

CMD java -jar target/api-0.0.1-SNAPSHOT.jar && tail -F /dev/null
package com.example.launder.presta.providerDetail;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProviderDetailRepository extends MongoRepository<ProviderDetail, String> {
    List<ProviderDetail> findByProviderId(@Param("providerId") String providerId);
}

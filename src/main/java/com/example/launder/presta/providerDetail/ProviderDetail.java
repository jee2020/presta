package com.example.launder.presta.providerDetail;

import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class ProviderDetail {
    private @Id String id;
    private String providerId;
    private String startingHourAM;
    private String endingHourAM;
    private String startingHourPM;
    private String endingHourPM;
    private String phone;
    private String addressId;
}

package com.example.launder.presta.provider;

import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class Provider {
    private @Id String id;
    private String userId;
    private double rate;
    private String type;
}

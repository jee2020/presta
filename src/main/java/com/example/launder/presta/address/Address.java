package com.example.launder.presta.address;

import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class Address {
    private @Id String id;
    private String userId;
    private String name;
    private int streetNumber;
    private String streetName;
    private int zipCode;
    private String city;
}

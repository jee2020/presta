package com.example.launder.presta.user;

import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class User {
    private @Id String id;
    private String lastName;
    private String firstName;
    private String password;
    private String email;
    private String phone;
    private boolean active;
}
